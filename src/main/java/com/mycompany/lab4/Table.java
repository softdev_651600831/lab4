/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author HP
 */
public class Table {
    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player Player1;
    private Player Player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row, col;

    public Table(Player Player1, Player Player2) {
        this.Player1 = Player1;
        this.Player2 = Player2;
        this.currentPlayer = Player1;
    }

    public char[][] getTable() {
        return table;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }

        return false;
    }

    public boolean checkWin() {
        if (checkCol() || checkRow() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][i] != currentPlayer.getSymbol()) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if ((table[0][2] == currentPlayer.getSymbol()) && (table[1][1] == currentPlayer.getSymbol()) && (table[2][0] == currentPlayer.getSymbol())) {
                return true;
            }
        }
        return false;
    }

    Player getCurrentPlayer() {
        return currentPlayer;
    }

    void switchPlayer() {
        turnCount++;
        if (currentPlayer == Player1) {
            currentPlayer = Player2;
        } else {
            currentPlayer = Player1;
        }
    }

    boolean checkDraw() {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (table[i][j] == '-') {
                        return false;
                    }
                }
            }
        return true;
    }
    
}
